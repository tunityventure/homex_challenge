<h1>HOMEX take-home challenge</h1>

<h2>Technical assessment description</h2>
<a href="https://docs.google.com/document/d/1UMRAZube74kcwT1XZFqK3rWUNFVODrqBDvhcOY_Ov2g/edit?usp=sharing" target="_blank">https://docs.google.com/document/d/1UMRAZube74kcwT1XZFqK3rWUNFVODrqBDvhcOY_Ov2g/edit?usp=sharing</a>


<h2>Solution</h2>

I decided to utilize a managed database solution for this assignment (PostgreSQL database cluster on DigitalOcean). I created a database for this project (`homex_db`) and wrote a Python script that populate two tables (`wb_countries`, `rc_countries`) and a view (`wb_rc_countries_joined_view`).

The Python script grabs data from two API sources (`REST Countries` and `World Bank Country`) and populates data onto this managed database solution. The Python script was containerized inside a Docker container for reproducibility.

`poetry`, a popular Python package management tool, was used to manage and install Python packages necessary for the script to run. 




<h2>How to run the task</h2>

1. Clone this repository and name it `homex`.
2. Place .env file (that is shared with you privately) inside `homex/homex` (inside the directory where `script.py` is located)
3. Create an empty `logs` directory inside `homex/homex` (inside the directory where `script.py` is located)
4. Install Docker on your local machine and start Docker.
5. Run run_docker.sh script of this repository through a command line tool. (e.g. by executing `bash ~/desktop/homex/run_docker.sh`)
6. Check that task was completed successfully by checking the logs in the `homex/homex/logs` directory. The script also sends an alert message to a <a href="https://tunityventure.slack.com/archives/C03BJ99MASK" target="_blank">Slack channel</a> upon completion of the task to notify whether the task was completed successfully or errored out. You can check the status of the task from the Slack channel.

With proper setup, your folder structure should look like this:

<pre>
.
├── README.md
├── docker-compose.yml
├── homex
│   ├── .env    ## <=== a private file placed manually ##
│   ├── Dockerfile
│   ├── functions
│   │   ├── api_functions.py
│   │   └── slack_message.py
│   ├── homex.Rproj
│   ├── logs    ## <=== directory created manually ##
│   ├── poetry.lock
│   ├── pyproject.toml
│   └── script.py
├── imgs
│   ├── database_schema_diagram.png
│   └── directory_view.png
└── run_docker.sh
</pre>


<h2>What the Python script does</h2>

The Python script grabs countries data from two API sources: `REST Countries API` and `World Bank Country API`. Data from World Bank API is "flattened" and stored into `wb_countries` table (no JSON fields) while data from REST Countries API is stored into `rc_countries`.

After tables are populated, the script creates a view called `wb_rc_countries_joined_view` that joins the above two tables to demonstrate that the two tables are joinable.

After these tables and the view are created, the script sends an alert message to a <a href="https://tunityventure.slack.com/archives/C03BJ99MASK" target="_blank">Slack channel</a> to notify whether or not the script ran successfully. Included throughout the script are `print()` statements, included for logging and debugging purposes. Those `print()` statements are preserved in `homex/homex/logs` folder.


<h2>What does the dockerization process look like (in order)?</h2>

- Create necessary directories (e.g. `/homex` and `/homex/logs`) inside a Docker container
- Copy files from local machine to Docker container (e.g. `script.py`, `poetry.lock`, etc.)
- Install `poetry` for Python package management
- Install necessary version-specific Python packages as defined in `poetry.lock`
- Set proper timezone so the logged timestamps can be read in local time (instead of UTC time)
- Execute Python script (`script.py`) that grabs data from API sources and populate tables & a view in the target database cluster



<h2>Database schema diagram</h2>
<img src="imgs/database_schema_diagram.png" />



<h2>Architecture diagram</h2>

<a href="https://docs.google.com/drawings/d/1Pd5haOoSzqDpc6p7vOid-ssNBspjG1UFSd5O5wfCASI/edit?usp=sharing" target="_blank">
    <img src="imgs/architecture_diagram.jpg" />
</a>