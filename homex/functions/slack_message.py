import requests

def send_slack_message(slack_webhook_url, message):
    payload = '{"text": "%s"}' % message
    response = requests.post(
      url = slack_webhook_url,
      data = payload
    )
  
    #print(response.text)
    return response