import requests
import time


## function to get country data from worldblank API url
def get_country_data_from_worldbank_url(url):
  
  ## make request to URL
  response = requests.get(url)
  
  ## get JSON data as dictionary
  data = response.json()
  
  ## get a list of country records from data dictionary
  country_records = data[1]
  
  ## initialize a list of country records to return
  country_records_flattened = []

  ## for each country record
  for country_record in country_records:
    
    ## create a flattened dictionary of country record
    country_record_flattened = {
      "id": country_record['id'],
      "iso2code": country_record['iso2Code'],
      "name": country_record['name'],
      "region_id": country_record['region']['id'],
      "region_iso2code": country_record['region']['iso2code'],
      "region_value": country_record['region']['value'],
      "admin_region_id": country_record['adminregion']['id'],
      "admin_region_iso2code": country_record['adminregion']['iso2code'],
      "admin_region_value": country_record['adminregion']['value'],
      "income_level_id": country_record['incomeLevel']['id'],
      "income_level_iso2code": country_record['incomeLevel']['iso2code'],
      "income_level_value": country_record['incomeLevel']['value'],
      "lending_type_id": country_record['lendingType']['id'],
      "lending_type_iso2code": country_record['lendingType']['iso2code'],
      "lending_type_value": country_record['lendingType']['value'],
      "capital_city": country_record['capitalCity'],
      "longitude": country_record['longitude'],
      "latitude": country_record['latitude'],
    }
    
    ## append to the list to return
    country_records_flattened.append(country_record_flattened)

  ## return list of flattened country records
  return country_records_flattened



## function to get country data from worldblank API urls of given page indices
def get_country_data_from_worldbank(page_indices):
  
  ## initialize a list of flattened country records
  country_records_flattened = []
  
  ## for each page index
  for page_index in page_indices:
    
    ## create request url
    api_request_url = f"https://api.worldbank.org/v2/country?format=json&page={page_index}"
    
    ## print log
    print(f"Accessing data from {api_request_url}")
    
    ## get list of country records from request url
    country_records = get_country_data_from_worldbank_url(api_request_url)
    
    ## add elements to the main list
    country_records_flattened += country_records
    
    ## wait
    time.sleep(1)

  ## return data   
  return country_records_flattened

  # ## convert list of dictionaries to data frame
  # df = pd.DataFrame.from_dict(country_records_flattened)
  # 
  # ## return data
  # return df  



## function to get country data from RestCountries API
def get_country_data_from_restcountries(country_codes_list):

  ## create a string of country codes separated by comma
  comma_separated_codes_str = ','.join(country_codes_list)
  
  ## create API request url
  restcountries_request_url = f"https://restcountries.com/v3.1/alpha?codes={comma_separated_codes_str}"
  
  ## print log
  print(f"Accessing data from {restcountries_request_url}")

  ## make request to URL
  response = requests.get(restcountries_request_url)
  
  ## get JSON data as dictionary
  data = response.json()
  
  ## return 
  return data
