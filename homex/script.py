## import libraries
import pandas as pd
import numpy as np
import datetime
import os
from dotenv import load_dotenv, find_dotenv
import sqlalchemy
from sqlalchemy import create_engine
#import mysql.connector  


## set working directory
#os.chdir("/Users/howards/desktop/homex/homex")  # for local machine environment (when running it "manually")
os.chdir("/homex")  # for Docker environment (when running it through Docker)


## import functions
from functions.slack_message import send_slack_message
from functions.api_functions import *




if __name__ == "__main__":
  
  ## print start statement
  print(f"Starting {os.path.basename(__file__)} at {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}.")
  

  ## print log  
  print("Current working directory: " + os.getcwd())

  
  ## load environmental variables
  load_dotenv(find_dotenv())
  #load_dotenv(".env")
  
  
  
  ## load DB credentials from environment variables
  DB_HOST = os.getenv("DB_HOST")
  DB_DATABASE = os.getenv("DB_DATABASE")
  DB_PORT = os.getenv("DB_PORT")
  DB_USERNAME = os.getenv("DB_USERNAME")
  DB_PASSWORD = os.getenv("DB_PASSWORD")
  
  ## load slack webhook url (for messaging)
  SLACK_WEBHOOK_URL = os.getenv("SLACK_WEBHOOK_URL")
  
  
  
  
  ## pandas display settings
  pd.set_option('display.max_rows', 500)
  pd.set_option('display.max_columns', 500)
  

  try:

    ######################################
    ##### create database connection #####
    ######################################
    

    ## create database connection (using mysql-connector-python)
    # db_connection = mysql.connector.connect(
    #   host = 'localhost', 
    #   database = 'public', 
    #   user = MYSQL_USER, 
    #   password = MYSQL_PASSWORD
    # )
    
    ## create database engine object
    engine = create_engine(f"postgresql://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_DATABASE}?sslmode=require")
    
    ## create database connection
    connection = engine.connect()





    ## drop target view if it already exists;
    ## we need this step because we cannot recreate/replace tables if this view exists;
    connection.execute("""
      drop view if exists wb_rc_countries_joined_view;
    """)
    
    
    
    
    
    ##########################################
    ##### upload data from WorldBank API #####
    ##########################################
    
    ## get flattened list of country records from WorldBank API
    country_records_flattened = get_country_data_from_worldbank(page_indices = list(range(1, 7)))
    
    ## convert list of dictionaries to data frame
    wb_df = pd.DataFrame.from_dict(country_records_flattened)
    
    ## replace field that's entirely space (or empty) with NaN
    wb_df.replace(r'^\s*$', np.nan, regex=True, inplace=True)
    
    ## upload data to database
    wb_df.to_sql(
        name = 'wb_countries',
        con = connection, 
        if_exists = 'replace', 
        index = False, 
    )
    
    
    
    
    ###############################################
    ##### upload data from REST Countries API #####
    ###############################################
    
    ## get data (list of dictionaries) from REST Countries API
    rc_data = get_country_data_from_restcountries(country_codes_list = list(wb_df.iso2code))
    
    ## convert list of dictionaries to data frame
    rc_df = pd.DataFrame(rc_data)
    
    ## rename columns to snake-case
    rc_df.columns = ['name', 'tld', 'cca2', 'ccn3', 'cca3', 'cioc', 'independent', 'status',
           'un_member', 'currencies', 'idd', 'capital', 'alt_spellings', 'region',
           'subregion', 'languages', 'translations', 'latlng', 'landlocked',
           'borders', 'area', 'demonyms', 'flag', 'maps', 'population', 'gini',
           'fifa', 'car', 'timezones', 'continents', 'flags', 'coat_of_arms',
           'start_of_week', 'capital_info', 'postal_code']
    
    ## upload data to database
    rc_df.to_sql(
        name = 'rc_countries',
        con = connection, 
        if_exists = 'replace', 
        index = False, 
    
        ## https://stackoverflow.com/questions/41469430/writing-json-column-to-postgres-using-pandas-to-sql
        dtype = {
          "name": sqlalchemy.types.JSON,
          "tld": sqlalchemy.types.JSON,
          "currencies": sqlalchemy.types.JSON,
          "idd": sqlalchemy.types.JSON,
          "capital": sqlalchemy.types.JSON,
          "alt_spellings": sqlalchemy.types.JSON,
          "languages": sqlalchemy.types.JSON,
          "translations": sqlalchemy.types.JSON,
          "latlng": sqlalchemy.types.JSON,
          "borders": sqlalchemy.types.JSON,
          "demonyms": sqlalchemy.types.JSON,
          "maps": sqlalchemy.types.JSON,
          "gini": sqlalchemy.types.JSON,
          "car": sqlalchemy.types.JSON,
          "timezones": sqlalchemy.types.JSON,
          "continents": sqlalchemy.types.JSON,
          "flags": sqlalchemy.types.JSON,
          "coat_of_arms": sqlalchemy.types.JSON,
          "capital_info": sqlalchemy.types.JSON,
          "postal_code": sqlalchemy.types.JSON,
        }
    )
    
    ## create view that joins wb_countries and rc_countries tables
    connection.execute("""
      create or replace view wb_rc_countries_joined_view as (
      	select 
      		a.iso2code,
      		a.name as wb_name,
      		b.cca2,
      		b.name as rc_name
      	from wb_countries as a
      	left join rc_countries as b
      	on a.iso2code = b.cca2
      );
    """)
    
    ## success message
    success_message = f"Countries data tables populated successfully at {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}!"
    
    ## send success message via Slack
    send_slack_message(
      slack_webhook_url = SLACK_WEBHOOK_URL,
      message = success_message
    )
    
    ## end database connection
    engine.dispose()
    
    ## print log
    print(success_message)

  except Exception as e:
    
    ## error message
    error_message = f"Script failure for {os.path.basename(__file__)} at {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {e}"
    
    ## send error message via Slack
    send_slack_message(
      slack_webhook_url = SLACK_WEBHOOK_URL,
      message = error_message
    )
    
    ## print log
    print(error_message)
  
  
  ## print start statement
  print(f"Ending {os.path.basename(__file__)} at {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}.")




